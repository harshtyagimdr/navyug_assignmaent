import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import User from './components/User/user';
import UserContextProvider from './contexts/user';
import UserDetails from './components/User/userDetails';
import PostContentProvider from './contexts/Post';
import 'bootstrap/dist/css/bootstrap.min.css';
import PostDetails from './components/Posts/PostDetails';
import CommentContentProvider from './contexts/Comment';

function App() {
  return (
    <UserContextProvider>
    <PostContentProvider>
    <CommentContentProvider>
    <BrowserRouter>
    <div className="App">
    <Switch>
      <Route exact  path="/" component={User}></Route>
      <Route exact  path="/user/:id" component={UserDetails}></Route>
      <Route exact  path="/user/:id/:pid" component={PostDetails}></Route>
    </Switch>
    </div>
    </BrowserRouter>
    </CommentContentProvider>
    </PostContentProvider>
    </UserContextProvider>
  );
}

export default App;
