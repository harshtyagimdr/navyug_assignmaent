import React,{createContext,useReducer,useEffect} from 'react';
import { postReducer } from '../reducers/postReducer';

export const PostContext=createContext();

const PostContentProvider=(props)=>{
    const [posts,dispatch]=useReducer(postReducer,[],()=>{
        const localData=localStorage.getItem('posts');
        return localData?JSON.parse(localData):[
            {id:1,uid:1,title:"Et duis magna magna cupidatat minim nostrud excepteur et.",body:"Minim excepteur et officia id enim reprehenderit tempor dolore nostrud cillum nostrud. Duis culpa ex dolore veniam cupidatat. Cillum non veniam eiusmod fugiat consectetur ullamco ipsum culpa consectetur ipsum nisi eiusmod. Deserunt excepteur nostrud duis sunt adipisicing pariatur Lorem exercitation. Sint ea occaecat sit quis incididunt deserunt incididunt elit amet. Incididunt ullamco nostrud minim culpa. Minim reprehenderit sunt esse elit aute dolore aliquip elit pariatur in cupidatat sit esse quis."},
            {id:2,uid:1,title:"Et duis magna magna cupidatat minim.",body:"Minim excepteur et officia id enim reprehenderit tempor dolore nostrud cillum nostrud. Duis culpa ex dolore veniam cupidatat. Cillum non veniam eiusmod fugiat consectetur ullamco ipsum culpa consectetur ipsum nisi eiusmod. Deserunt excepteur nostrud duis sunt adipisicing pariatur Lorem exercitation. Sint ea occaecat sit quis incididunt deserunt incididunt elit amet. Incididunt ullamco nostrud minim culpa. Minim reprehenderit sunt esse elit aute dolore aliquip elit pariatur in cupidatat sit esse quis."},
            {id:3,uid:1,title:"Et duis magna magna cupidatat minim nostrud .",body:"Minim excepteur et officia id enim reprehenderit tempor dolore nostrud cillum nostrud. Duis culpa ex dolore veniam cupidatat. Cillum non veniam eiusmod fugiat consectetur ullamco ipsum culpa consectetur ipsum nisi eiusmod. Deserunt excepteur nostrud duis sunt adipisicing pariatur Lorem exercitation. Sint ea occaecat sit quis incididunt deserunt incididunt elit amet. Incididunt ullamco nostrud minim culpa. Minim reprehenderit sunt esse elit aute dolore aliquip elit pariatur in cupidatat sit esse quis."},
        ];
    });
    useEffect(()=>{
        localStorage.setItem('posts',JSON.stringify(posts))
    },[posts]);
    return(
        <PostContext.Provider value={{posts,dispatch}}>
            {props.children}
        </PostContext.Provider>
    )
}
export default PostContentProvider;