import React,{useState,createContext} from 'react';

export const UserContext=createContext();
const UserContextProvider = (props) => {
   const [users,setUsers]= useState([
        {id:1,name:"Harsh Tyagi",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
        {id:2,name:"Harsh ",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
        {id:3,name:"Shivam Tyagi",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
        {id:4,name:"Shivam",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
        {id:5,name:"Vipul",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
        {id:6,name:"Vipul Tyagi",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
        {id:7,name:"Gaurav",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
        {id:8,name:"Gaurav Tyagi",email:"harshtyagi@gmail.com",website:"harsh.com",address:"Ghaziabad",phone:"8979749001"},
    ])
    return ( 
        <UserContext.Provider value={{users}}>
            {props.children}
        </UserContext.Provider>
     );
}
 
export default UserContextProvider;