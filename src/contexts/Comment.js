import React,{createContext,useEffect,useReducer} from 'react';
import { commentReducer } from '../reducers/commentReducer';


export const CommentContext=createContext();

const CommentContentProvider=(props)=>{
    const [comments,dispatch]=useReducer(commentReducer,[],()=>{
        const localData=localStorage.getItem('comments');
        return localData?JSON.parse(localData):[
            {id:1,pid:1,name:"HarshTyagi",email:"harshtyagi@gmail.com",body:"Id ex velit officia ea anim. Ad Lorem aliquip culpa est eu ut magna nostrud. Consectetur sit amet excepteur proident occaecat. Et eiusmod qui proident do velit duis ex commodo sit nulla laborum do. Est nisi ipsum non mollit pariatur proident cupidatat. Esse ut ut nulla sit voluptate dolore duis nulla irure nostrud sunt exercitation. Nostrud laboris culpa veniam sunt."},
            {id:2,pid:1,name:"HarshTyagi",email:"harshtyagi@gmail.com",body:"Id ex velit officia ea anim. Ad Lorem aliquip culpa est eu ut magna nostrud. Consectetur sit amet excepteur proident occaecat. Et eiusmod qui proident do velit duis ex commodo sit nulla laborum do. Est nisi ipsum non mollit pariatur proident cupidatat. Esse ut ut nulla sit voluptate dolore duis nulla irure nostrud sunt exercitation. Nostrud laboris culpa veniam sunt."},
            {id:3,pid:1,name:"HarshTyagi",email:"harshtyagi@gmail.com",body:"Id ex velit officia ea anim. Ad Lorem aliquip culpa est eu ut magna nostrud. Consectetur sit amet excepteur proident occaecat. Et eiusmod qui proident do velit duis ex commodo sit nulla laborum do. Est nisi ipsum non mollit pariatur proident cupidatat. Esse ut ut nulla sit voluptate dolore duis nulla irure nostrud sunt exercitation. Nostrud laboris culpa veniam sunt."},
        ];
    }
    );
    useEffect(()=>{
        localStorage.setItem('comments',JSON.stringify(comments))
    },[comments]);
    return(
        <CommentContext.Provider value={{comments,dispatch}}>
            {props.children}
        </CommentContext.Provider>
    )
}
export default CommentContentProvider;