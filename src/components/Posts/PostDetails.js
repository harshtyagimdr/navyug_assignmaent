import React,{useContext,useState} from 'react';
import { useParams,Link } from "react-router-dom";
import {PostContext} from '../../contexts/Post'
import { UserContext } from '../../contexts/user';
import {CommentContext} from '../../contexts/Comment';
import CommentDetails from '../Comments/CommentDetails';
import { Modal,Button,Form } from 'react-bootstrap';

const PostDetails = () => {
    const { pid ,id} = useParams();
    const {posts}=useContext(PostContext);
    const {users}=useContext(UserContext);
    const {comments,dispatch}=useContext(CommentContext);
    const post=posts.filter(post=>post.id==pid);
    const user=users.filter(user=>user.id==id);
    const post_comments=comments.filter(comment=>pid==comment.pid);
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const[name,setName]=useState('');
    const[email,setEmail]=useState('');
    const[body,setBody]=useState('');
    // console.log(post_comments)
    const [showComments,setshowComments]=useState(false); 
    const handleSubmit=(e)=>{
        e.preventDefault();

         dispatch({type:"ADD_COMMENT",comment:{
          name,email,body,pid
         }})
        setName('');
        setEmail('');
        setBody('');
        setShow(false);
    } 
    return ( 
        <div className="container my-5">
              <Modal show={show} onHide={handleClose} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Add Comment</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form onSubmit={handleSubmit}>
                    <Form.Group controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter name" value={name} onChange={(e)=>setName(e.target.value)} required/>
                        
                    </Form.Group>
                    <Form.Group controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e)=>setEmail(e.target.value)} />
                        
                    </Form.Group>

                    <Form.Group controlId="body">
                        <Form.Label>Body</Form.Label>
                        <Form.Control as="textarea" rows="3" value={body} onChange={(e)=>setBody(e.target.value)} />
                    </Form.Group>
                    <Button variant="primary mx-2" type="submit">
                        Save
                    </Button>
                    <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
              
                </Modal.Footer>
            </Modal>
             <div className="row">
                    <div className="col-md-3">
                        <div className="row text-primary">
                        <Link to={"/user/"+id}>
                        <i className="fa fa-arrow-left fa-2x" aria-hidden="true" ></i>	&nbsp;
                        </Link>
                        Back
                        
                        </div>
                    </div>
                    <h3 className="col-md-6 text-center font-weight-bolder">{user[0].name}</h3>
                   
                </div>
                <div className="font-weight-bolder text-center mt-5 mb-2">{post[0].title}</div>
                <p>{post[0].body}</p>

                <div className="row">
                    <div className="float-left col-md-4">
                        {showComments?(<a className="text-primary" style={{cursor:"pointer",textDecoration:'underline'}} onClick={()=>setshowComments(false)}>Hide Comments</a>):(<a className="text-primary" style={{cursor:"pointer",textDecoration:'underline'}} onClick={()=>setshowComments(true)}>Show Comments</a>)}
                        
                    </div>
                    <div className="col-md-6"></div>
                    <div className="float-right col-md-2">
                    {showComments?(<a className="text-primary" style={{cursor:"pointer",textDecoration:'underline'}} onClick={handleShow}>Add comment</a>):(<></>)}
                        
                    </div>
                </div>
                {showComments && post_comments.length?(<ul className="list-group my-5"> 
                    {post_comments.map(comment=>{
                        return (
                           <CommentDetails comment={comment} key={comment.id}/>
                        )
                    })}
                </ul>):
                (<ul className="list-group">
                
                {showComments?(<li className="list-group-item text-center text-danger ">No Comments Available </li>):(<></>)}
                
                </ul>)}

        </div>
     );
}
 
export default PostDetails;