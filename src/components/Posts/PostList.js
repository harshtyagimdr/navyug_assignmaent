import React,{useContext} from 'react';
import {Link} from 'react-router-dom';
import { PostContext } from '../../contexts/Post';

const PostList = ({post}) => {
   const {dispatch}= useContext(PostContext);
//    console.log(post)
    return ( 
        <li className="list-group-item ">
        <div className="row">
       
        <i className="fa fa-trash fa-2x text-primary col-md-2" aria-hidden="true" style={{cursor:"pointer"}}  onClick={()=>dispatch({type:'REMOVE_POST',id:post.id})}></i>
      
        <div className=" col-md-9">{post.title.slice(0,100)}</div>
        <Link to={{pathname:'/user/'+post.uid+'/'+post.id}} className="col-md-1 float-right">
        <i className="fa fa-arrow-right fa-2x text-primary" aria-hidden="true"></i>
        </Link>
        </div>
    </li>
     );
}
 
export default PostList;