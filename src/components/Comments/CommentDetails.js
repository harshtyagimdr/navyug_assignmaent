import React from 'react';
const CommentDetails = ({comment}) => {
    console.log(comment)
    return ( 
        <li className="list-group-item my-2">
        <div className="row">
            <div className="col-md-6 font-weight-bolder">{comment.name}</div>       
            <div className="col-md-6 text-right text-primary" style={{textDecoration:'underline'}}>{comment.email}</div>       
        </div>
        <p>{comment.body}</p>
    </li>
     );
}
 
export default CommentDetails;