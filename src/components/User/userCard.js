import React from 'react';
import {Link} from 'react-router-dom';


export default function UserCard({user}) {
    return (
        <div>
            <div className="card mx-2 my-2" style={{width: '16rem'}}>
              <div className="card-body">
                <h5 className="card-title">{user.name}</h5>
                <Link  to="/">{user.phone}</Link><br></br>
                <Link  to="/">{user.email}</Link><br></br>
                <Link to="/" >{user.website}</Link>
                <p className="card-text">{user.address}</p>
                <div className="text-center">
                <Link to={{pathname:'/user/'+user.id, query:"harsh"}}  className="btn btn-secondary">
                Details
                </Link>
                </div>
            </div>
            </div>

        </div>
    )
}
