import React,{useContext,useState} from 'react';
import { useParams,Link } from "react-router-dom";
import {UserContext} from '../../contexts/user'
import { PostContext } from '../../contexts/Post';
import PostList from '../Posts/PostList';
import { Modal,Button,Form } from 'react-bootstrap';

const UserDetails=()=> {
    const {users}=useContext(UserContext);
    const {posts,dispatch} =useContext(PostContext);
    const { id } = useParams();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const[title,setTitle]=useState('');
    const[body,setBody]=useState('');
    
    const user=users.filter(user=>user.id == id);
    const user_posts=posts.filter(post=>post.uid==id);
    const handleSubmit=(e)=>{
        e.preventDefault();

         dispatch({type:"ADD_POST",post:{
           title,body,uid:id
         }})
        setTitle('');
        setBody('');
        setShow(false);
    }
    // console.log(user_posts)
    // console.log(user[0].name)
        return (
            <div className="container my-5">
            <Modal show={show} onHide={handleClose} animation={false}>
                <Modal.Header closeButton>
                <Modal.Title>Add Post</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form onSubmit={handleSubmit}>
                    <Form.Group controlId="title">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" placeholder="Enter title" value={title} onChange={(e)=>setTitle(e.target.value)} required/>
                        
                    </Form.Group>

                    <Form.Group controlId="body">
                        <Form.Label>Body</Form.Label>
                        <Form.Control as="textarea" rows="3" value={body} onChange={(e)=>setBody(e.target.value)} required/>
                    </Form.Group>
                    <Button variant="primary mx-2" type="submit">
                        Save
                    </Button>
                    <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
              
                </Modal.Footer>
            </Modal>
                <div className="row">
                    <div className="col-md-3">
                        <div className="row text-primary">
                        <Link to="/">
                        <i className="fa fa-arrow-left fa-2x" aria-hidden="true" style={{ color: 'inherit', textDecoration: 'none'}} ></i>	&nbsp;
                        </Link>
                        Back
                        
                        </div>
                    </div>
                    <h3 className="col-md-6 text-center font-weight-bolder">{user[0].name}</h3>
                    <div className="col-md-3 text-primary">
                    <i className="fa fa-plus-circle fa-2x float-right" aria-hidden="true"  style={{cursor:"pointer"}} onClick={handleShow}></i>
                    </div>
                </div>
                <div className="container my-5">
                {user_posts.length?(<ul className="list-group"> 
                    {user_posts.map(post=>{
                        return (
                            <PostList post={post} key={post.id}/>
                        )
                    })}
                </ul>):
                (<ul className="list-group">
                <li className="list-group-item text-center text-danger ">
                No Post Available</li> </ul>)}
                

                </div>
            </div>
        )
    }



 
export default UserDetails;