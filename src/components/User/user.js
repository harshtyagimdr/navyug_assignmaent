import React,{useContext} from 'react';
import { UserContext } from '../../contexts/user';
import UserCard from './userCard';


const User = () => {
    const {users}=useContext(UserContext);
    // console.log(users)
    return ( 
        <div className="container my-5">
            <div className="row">
       
            {users.map(user=>{
                return (
                    
                   <UserCard  key={user.id} user={user}/>
                )
            })}
        </div>
        </div>
     );
}
 
export default User;