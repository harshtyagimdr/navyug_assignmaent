import { v1 as uuidv1 } from 'uuid';
export const postReducer=(state,action)=>{
    switch(action.type){
        case 'ADD_POST':
            return [...state,{
                title:action.post.title,
                body:action.post.body,
                uid:action.post.uid,
                id:uuidv1(),
            }]
        case 'REMOVE_POST':
            return state.filter(post=>post.id !==action.id);
        default:
            return state;
    }
}