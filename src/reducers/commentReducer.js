import { v1 as uuidv1 } from 'uuid';
export const commentReducer=(state,action)=>{
    switch(action.type){
        case 'ADD_COMMENT':
            return [...state,{
                name:action.comment.name,
                email:action.comment.email,
                body:action.comment.body,
                pid:action.comment.pid,
                id:uuidv1(),
            }]
        
        default:
            return state;
    }
}